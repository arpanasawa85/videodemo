package com.demo.videoplayer.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<T: ViewDataBinding, V: BaseViewModel>: Fragment() {

    lateinit var mViewDataBinding : T
    lateinit var mViewModel: V

    // This can be accessed by the child fragments
    // Only valid between onCreateView and onDestroyView
    val binding : T get() = mViewDataBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
        this.mViewModel = if(!::mViewModel.isInitialized) getViewModel() else mViewModel
        bindVariable()
        mViewDataBinding.executePendingBindings()
        return binding.root
    }

    // Removing the binding reference when not needed is recommended as it avoids memory leak
    override fun onDestroyView() {
        super.onDestroyView()
    }

    abstract fun bindVariable()

    abstract fun getViewModel() : V

    abstract fun getLayout() : Int

}