package com.demo.videoplayer.base

import android.content.res.Configuration
import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.demo.videoplayer.R
import com.demo.videoplayer.databinding.ActivityBaseLayoutBinding
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.demo.videoplayer.util.DialogHelper as DialogHelper1

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {

    lateinit var mViewDataBinding: T
    lateinit var mViewModel: V
    private lateinit var baseBinding: ActivityBaseLayoutBinding

    private fun getBaseContainer(): FrameLayout {
        if (!::baseBinding.isInitialized) {
            baseBinding = ActivityBaseLayoutBinding.inflate(layoutInflater)
        }
        return baseBinding.baseContainer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        super.onCreate(savedInstanceState)
        initializeViewBinding()
        //setupNetworkIntentFilter()

        mViewModel.showProgressDialog.observe(this) {
            if (!it && DialogHelper1.isLoader) {
                DialogHelper1.hide()
            } else {
                DialogHelper1.showLoaderDialog(this)
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        initializeViewBinding()
    }

    private fun initializeViewBinding() {
        baseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base_layout)
        mViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, getLayout(), baseBinding.baseContainer, false)
        baseBinding.baseContainer.removeAllViews()
        baseBinding.baseContainer.addView(mViewDataBinding.root)
        this.mViewModel = if (!::mViewModel.isInitialized) getViewModel() else mViewModel
        bindVariable()
        mViewDataBinding.executePendingBindings()
    }

    fun requestPermission(permission: String, callback: (Boolean) -> Unit) {
        Dexter.withContext(this).withPermission(permission).withListener(
            object : PermissionListener {
                override fun onPermissionGranted(grantedResponse: PermissionGrantedResponse) {
                    callback(true)
                }

                override fun onPermissionDenied(deniedResponse: PermissionDeniedResponse) {
                    callback(false)
                }

                override fun onPermissionRationaleShouldBeShown(
                    request: PermissionRequest,
                    token: PermissionToken
                ) {
                    token.cancelPermissionRequest()
                }
            }
        ).check()
    }

    abstract fun bindVariable()

    abstract fun getViewModel(): V

    abstract fun getLayout(): Int

}