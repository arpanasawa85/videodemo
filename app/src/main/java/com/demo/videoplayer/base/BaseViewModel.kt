package com.demo.videoplayer.base

import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel() : ViewModel(), Observable {
/*
    val loggedOutEvent: MutableLiveData<Boolean> = MutableLiveData()

    val cacheProvider: CacheProvider = inject<CacheProvider>(CacheProvider::class.java).value
    val session: SessionRepository = inject<SessionRepository>(SessionRepository::class.java).value*/

    val showToast: MutableLiveData<String> = MutableLiveData()
    val showErrorDialog: MutableLiveData<String?> = MutableLiveData()
    val showProgressDialog: MutableLiveData<Boolean> = MutableLiveData()

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {}

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {}
}