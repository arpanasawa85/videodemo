package com.demo.videoplayer.util

import android.util.Log

class MLogger {
    val IS_LOG_ENABLED: Boolean = true
    fun debug(pTag: String?, pMessage: String) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.d("TAG DEBUG : $pTag", " message : $pMessage")
        }
    }

    fun error(pTag: String?, pException: Exception) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.e(
                "TAG ERROR : $pTag",
                if (" message : $pException" != null) "" + pException.message else ""
            )
        }
    }

    fun error(pTag: String?, pMessage: String) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.e("TAG ERROR : $pTag", " message : $pMessage")
        }
    }

    fun error(pTag: String?, mes: String, pException: Exception?) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.e("TAG ERROR : $pTag", "ErrorMessage : $mes", pException)
        }
    }

    fun info(pTag: String?, pMessage: String) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.i("TAG INFO : $pTag", " message : $pMessage")
        }
    }

    fun verbose(pTag: String?, pMessage: String) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.v("TAG VERBOSE : $pTag", " message : $pMessage")
        }
    }

    fun warn(pTag: String?, pMessage: String) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            Log.w("TAG WARN : $pTag", " message : $pMessage")
        }
    }

    fun println(pTag: String?, pMessage: String) {
        var pTag = pTag
        if (IS_LOG_ENABLED) {
            if (pTag == null) pTag = ""
            println("$pTag Message-----> $pMessage")
        }
    }

    fun withLogLevel(values: Int): Int? {
        verbose("Tag", values.toString())
        return values
    }

    fun log(tag: String?, level: Int, t: Throwable?, vararg messages: Any) {
        if (Log.isLoggable(tag, level) && IS_LOG_ENABLED) {
            val message: String
            message = if (t == null && messages != null && messages.size == 1) {
                // handle this common case without the extra cost of creating a stringbuffer:
                messages[0].toString()
            } else {
                val sb = StringBuilder()
                if (messages != null) for (m in messages) {
                    sb.append(m)
                }
                if (t != null) {
                    sb.append("\n").append(Log.getStackTraceString(t))
                }
                sb.toString()
            }
            Log.println(level, tag, message)
        }
    }
}