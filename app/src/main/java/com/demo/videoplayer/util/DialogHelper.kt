package com.demo.videoplayer.util

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.demo.videoplayer.R
import com.demo.videoplayer.databinding.GenericLoadingDialogBinding


object DialogHelper {

    private var currentDialog: Dialog? = null
    val isShowing: Boolean
        get() {
            return currentDialog != null
        }
    var isLoader: Boolean = false
        private set


    fun showProgressDialog(context: Context, message: String? = null) {

        if (currentDialog != null) {
            currentDialog!!.hide()
            currentDialog!!.dismiss()
            currentDialog = null
        }

        val llPaddingHorizontal = 30
        val llPaddingVertical = 50
        val progressPadding = 40
        val ll = LinearLayout(context)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(
            llPaddingHorizontal,
            llPaddingVertical,
            llPaddingHorizontal,
            llPaddingVertical
        )
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(context)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, progressPadding, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(context)
        tvText.text = "$message"
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)

        if (message != null)
            ll.addView(tvText)

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setCancelable(true)
        builder.setView(ll)

        val dialog: AlertDialog = builder.create()
        currentDialog = dialog
        if (currentDialog == dialog) {
            dialog.show()
            val window: Window? = dialog.window
            if (window != null) {
                val layoutParams = WindowManager.LayoutParams()
                layoutParams.copyFrom(dialog.window!!.attributes)
                layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
                layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
                dialog.window!!.attributes = layoutParams
            }
        }

        dialog.setOnCancelListener {
            currentDialog = null
        }
        isLoader = false

    }


    fun showAlertDialog(
        context: Context,
        title: String? = null,
        message: String? = null,
        cancelable: Boolean = false,
        positive: String? = null,
        negative: String? = null,
        positiveCallback: (() -> Unit)? = null,
        negativeCallback: (() -> Unit)? = null
    ) {

        if (currentDialog != null) {
            currentDialog!!.hide()
            currentDialog!!.dismiss()
            currentDialog = null
        }

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        if (message != null) builder.setMessage(message)
        if (title != null) builder.setTitle(title)
        builder.setCancelable(cancelable)
        if (positive != null)
            builder.setPositiveButton(
                positive
            ) { p0, p1 ->
                if (positiveCallback != null) {
                    positiveCallback()
                }

            }
        if (negative != null)
            builder.setNegativeButton(negative) { p0, p1 ->
                if (negativeCallback != null) {
                    negativeCallback()
                }
            }
        val dialog = builder.create()
        currentDialog = dialog
        if (currentDialog == dialog) {
            dialog.show()
        }

        dialog.setOnCancelListener {
            currentDialog = null
        }
        isLoader = false

    }

    fun showCustomDialog(context: Context, view: View) {
        if (currentDialog != null) {
            currentDialog!!.hide()
            currentDialog!!.dismiss()
            currentDialog = null
        }

        val dialog = Dialog(context)
        dialog.setContentView(view)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.decorView?.setBackgroundColor(Color.TRANSPARENT)
        dialog.setCancelable(false)

        currentDialog = dialog
        if (currentDialog == dialog) {
            dialog.show()
        }
        isLoader = false

    }

    fun showLoaderDialog(context: Context, color: Int = R.color.primary) {
        if (currentDialog != null) {
            currentDialog!!.hide()
            currentDialog!!.dismiss()
            currentDialog = null
        }

        val dialog = Dialog(context)
        val view = GenericLoadingDialogBinding.inflate(LayoutInflater.from(context))
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
            view.dialogLoadingProgressIndicator.setIndicatorColor(context.getColor(color))
        dialog.setContentView(view.root)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.decorView?.setBackgroundColor(Color.TRANSPARENT)
        dialog.setCancelable(false)
        currentDialog = dialog
        if (currentDialog == dialog) {
            dialog.show()
        }
        isLoader = true
    }

    fun hide() {
        currentDialog?.hide()
        currentDialog!!.dismiss()
        currentDialog = null
        isLoader = false
    }

}