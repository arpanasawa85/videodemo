package com.digispice.superapp

import android.app.Application
import com.demo.videoplayer.di.module.appModule
import com.demo.videoplayer.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin

class DemoApplication: Application() {

    override fun onCreate() {
        instance = this
        super.onCreate()

        stopKoin()
        startKoin {
            androidContext(this@DemoApplication)
            modules(
                listOf(
                    appModule,
                    viewModelModule,
                )
            )
        }
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }

    companion object {
        lateinit var instance: DemoApplication
            private set
    }

}