package com.demo.videoplayer.data

import android.content.ContentUris
import android.net.Uri
import android.os.Environment

import android.os.Parcelable
import android.provider.MediaStore

import kotlinx.parcelize.Parcelize

import androidx.annotation.Keep

@Keep
@Parcelize
open class VideoData(var id: Long, var displayName: String, var contentUri: Uri, var duration: Int, var size: Long, var isLocal: Boolean = false): Parcelable{

}