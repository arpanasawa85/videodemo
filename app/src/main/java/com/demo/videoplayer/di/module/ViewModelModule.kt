package com.demo.videoplayer.di.module

import com.demo.videoplayer.view.viewmodel.MainViewModel
import com.demo.videoplayer.view.viewmodel.HomeViewModel
import com.demo.videoplayer.view.viewmodel.SplashViewModel
import com.demo.videoplayer.view.viewmodel.VideoGalleryViewModel
import com.demo.videoplayer.view.viewmodel.VideoPlayerViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { VideoGalleryViewModel(get()) }
    viewModel { VideoPlayerViewModel(get()) }
    viewModel { MainViewModel(get()) }
    /*viewModel { WebHomeViewModel() }
    viewModel { params -> WebViewModel(params.get(), get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { params -> VideoPlayerViewModel(params.get(), get()) }
    viewModel { params -> ChatViewModel(get()) }
    viewModel { AudioPlayerViewModel(get()) }*/

}