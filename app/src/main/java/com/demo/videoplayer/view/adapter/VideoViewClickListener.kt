package com.demo.videoplayer.view.adapter

import android.widget.VideoView

interface VideoViewClickListener {
    fun rightClick(videoView: VideoView)
    fun leftClick(videoView: VideoView)
    fun viewClick(videoView: VideoView)
}