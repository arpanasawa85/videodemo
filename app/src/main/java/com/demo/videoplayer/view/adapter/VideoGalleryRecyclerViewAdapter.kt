package com.demo.videoplayer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.demo.videoplayer.R
import com.demo.videoplayer.data.VideoData
import com.demo.videoplayer.databinding.ItemVideoViewBinding


class VideoGalleryRecyclerViewAdapter(
    private var items: List<VideoData>
) :
    RecyclerView.Adapter<VideoGalleryRecyclerViewAdapter.ViewHolder>() {

    private var onRecyclerItemClickedListener: OnRecyclerItemClickedListener<VideoData>? = null
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemVideoViewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_video_view,
            parent,
            false
        )
        context = parent.context
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = items[position]
        try {
            Glide
                .with(context)
                .asBitmap()
                .load(item.contentUri)
                .into(holder.binding.ivVideoThumnail);
        } catch (e: Exception) {
        }
        holder.binding.tvVideoName.text = item.displayName

        holder.binding.tvVideoDuration.text = getTime(item.duration)

        holder.binding.root.setOnClickListener {
            onRecyclerItemClickedListener?.onClicked(position, item)
        }
    }

    fun getTime(duration: Int): String {
        var time = ""
        var hours = ((duration / 1000) / 60) / 60
        var minutes = if (hours > 0) ((duration / 1000) / 60) % 60 else ((duration / 1000) / 60)
        var seconds = (duration / 1000) % 60

        var hrs = if (hours > 9) "" + hours else "0" + hours
        var min = if (minutes > 9) "" + minutes else "0" + minutes
        var sec = if (seconds > 9) "" + seconds else "0" + seconds
        time = if (hours > 0) (hrs + ":" + min + ":" + sec) else (min + ":" + sec)
        return time
    }

    override fun getItemCount(): Int = items.size

    fun setonRecyclerItemClickedListener(listener: OnRecyclerItemClickedListener<VideoData>?) {
        onRecyclerItemClickedListener = listener
    }

    class ViewHolder(@NonNull val binding: ItemVideoViewBinding) :
        RecyclerView.ViewHolder(binding.root)
}