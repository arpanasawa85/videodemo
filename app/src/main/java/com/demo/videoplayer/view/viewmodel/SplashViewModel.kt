package com.demo.videoplayer.view.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import com.demo.videoplayer.base.BaseViewModel

class SplashViewModel(
    @SuppressLint("StaticFieldLeak") private val context: Context,
) : BaseViewModel() {
}