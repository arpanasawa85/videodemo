package com.demo.videoplayer.view.fragment

import android.os.Bundle
import android.view.View
import com.demo.videoplayer.R
import com.demo.videoplayer.base.BaseFragment
import com.demo.videoplayer.databinding.FragmentMainBinding
import com.demo.videoplayer.view.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment<FragmentMainBinding, MainViewModel>() {

    var title: String? = ""
    var colorCode: Int? = 0

    companion object {
        var TTILE = "title"
        var COLOR_CODE = "colorCode"
        fun newInstance() = MainFragment()
    }

    override fun bindVariable() {
        mViewDataBinding.lifecycleOwner = viewLifecycleOwner
    }

    override fun getViewModel(): MainViewModel {
        val viewModel: MainViewModel by viewModel()
        return viewModel
    }

    override fun getLayout(): Int = R.layout.fragment_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            title = arguments!!.getString(MainFragment.TTILE)
            colorCode = arguments!!.getInt(MainFragment.COLOR_CODE)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding.message.text = title!!
        mViewDataBinding.main.setBackgroundColor(colorCode!!)
    }
}