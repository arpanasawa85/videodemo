package com.demo.videoplayer.view.viewmodel

import android.content.ContentUris
import android.content.Context
import android.provider.MediaStore
import android.provider.MediaStore.Video
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.demo.videoplayer.base.BaseViewModel
import com.demo.videoplayer.data.VideoData
import com.demo.videoplayer.util.MLogger
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class VideoGalleryViewModel(
    @android.annotation.SuppressLint("StaticFieldLeak") private val context: Context,
) : BaseViewModel() {

    val videoDataCompleted: MutableLiveData<List<VideoData>> = MutableLiveData()

    /*private var videoList: MutableLiveData<List<VideoData>> =
        MutableLiveData<List<VideoData>>().apply {
            value = listOf()
        }*/

    /*init {
        val list = ArrayList<VideoData>()
        list.add(VideoData("Beach Local Video", "sample_960x540.mp4", false, true))
        list.add(VideoData("Earth Local Video No Audio","Sample-Video-File-For-Testing.mp4",false, true))
        list.add(VideoData("Despacito Local Song","Luis%20Fonsi%20-%20Despacito%20ft.%20Daddy%20Yankee.mp3",false,true))
        list.add(VideoData("Bunny Local Video M3U8", "master.m3u8", false, true))
        list.add(VideoData("Animals Local VideoM3U8", "playlist.m3u8", false, true))
        list.add(VideoData("Flower Remote Video", "https://getsamplefiles.com/download/mp4/720"))
        list.add(VideoData("Apple Event Remote Video M3U8","http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8"))
        list.add(VideoData("Thor Trailer(Youtube)", "tgB1wUcmbbw", true))
        list.add(VideoData("Dr Strange Video(Youtube)", "3mSMUZ_aC14", true))
        videoList.value = list
    }*/

    fun getVideoList() {
        viewModelScope.launch {
            try {
                videoDataCompleted.postValue(getVideoData())
            } catch (e: Exception) {
                videoDataCompleted.postValue(arrayListOf())
            }
        }
    }

    fun getVideoData(): List<VideoData> {
        var videoList: ArrayList<VideoData> = ArrayList<VideoData>()

        val projection = arrayOf(
            MediaStore.Video.Media._ID,
            MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.DURATION,
            MediaStore.Video.Media.SIZE
        )
        val selection = MediaStore.Video.Media.DURATION + " >= ?"
        val selectionArgs = arrayOf<String>(
            java.lang.String.valueOf(TimeUnit.MILLISECONDS.convert(5, TimeUnit.MILLISECONDS))
        )
        val sortOrder = Video.Media.DATE_ADDED + " DESC"
        try {
            var cursor = context.getApplicationContext().getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                sortOrder
            )
            // Cache column indices.
            val idColumn = cursor!!.getColumnIndexOrThrow(Video.Media._ID)
            val nameColumn = cursor!!.getColumnIndexOrThrow(Video.Media.DISPLAY_NAME)
            val durationColumn = cursor!!.getColumnIndexOrThrow(Video.Media.DURATION)
            val sizeColumn = cursor!!.getColumnIndexOrThrow(Video.Media.SIZE)

            while (cursor.moveToNext()) {
                // Get values of columns for a given video.
                val id = cursor.getLong(idColumn)
                val displayName = cursor.getString(nameColumn)
                val duration = cursor.getInt(durationColumn)
                val size = cursor.getLong(sizeColumn)

                val contentUri = ContentUris.withAppendedId(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id
                )

                // Stores column values and the contentUri in a local object that represents the media file.
                videoList.add(VideoData(id, displayName, contentUri, duration, size, true))
            }
        } catch (e: Exception) {
            MLogger().error("VideoGalleryViewModel", e)
        }

        return videoList
    }
}