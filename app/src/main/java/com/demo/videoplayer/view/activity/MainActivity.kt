package com.demo.videoplayer.view.activity

import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.TranslateAnimation
import com.demo.videoplayer.R
import com.demo.videoplayer.base.BaseActivity
import com.demo.videoplayer.databinding.ActivityMainBinding
import com.demo.videoplayer.view.fragment.MainFragment
import com.demo.videoplayer.view.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    var width = 0
    var height = 0
    val ANIMATION_DURATION: Long = 1000

    override fun bindVariable() {
        mViewDataBinding.lifecycleOwner = this
    }

    override fun getViewModel(): MainViewModel {
        val viewModel: MainViewModel by viewModel()
        return viewModel
    }

    override fun getLayout(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {

            val firstBundle = Bundle()
            firstBundle.putString(MainFragment.TTILE, getString(R.string.first_fragment))
            firstBundle.putInt(MainFragment.COLOR_CODE, Color.YELLOW)

            val firstFragment = MainFragment.newInstance()
            firstFragment.setArguments(firstBundle)

            supportFragmentManager.beginTransaction()
                .replace(R.id.first_container, firstFragment)
                .commitNow()

            val secondBundle = Bundle()
            secondBundle.putString(MainFragment.TTILE, getString(R.string.second_fragment))
            secondBundle.putInt(MainFragment.COLOR_CODE, Color.GREEN)
            val secondFragment = MainFragment.newInstance()
            secondFragment.setArguments(secondBundle)

            supportFragmentManager.beginTransaction()
                .replace(R.id.second_container, secondFragment)
                .commitNow()
        }

        mViewDataBinding.firstContainer.setOnClickListener(View.OnClickListener {
            changeHeightWidth(mViewDataBinding.firstContainer, mViewDataBinding.secondContainer)
        })

        mViewDataBinding.secondContainer.setOnClickListener(View.OnClickListener {
            changeHeightWidth(mViewDataBinding.secondContainer, mViewDataBinding.firstContainer)
        })
    }

    fun changeHeightWidth(mainContainer: View, secondContainer: View) {
        val params: ViewGroup.LayoutParams = mainContainer.getLayoutParams()
        if (params.width == mViewDataBinding.main.width) {
            var targetWidth = width
            var targetHeight = height
            startAnimation(mainContainer, targetWidth, targetHeight)
            startAnimation(secondContainer, targetWidth, targetHeight)
        } else {
            width = params.width
            height = params.height
            var targetWidth = mViewDataBinding.main.width
            var targetHeight = mViewDataBinding.main.height;
            startAnimation(secondContainer, 0, 0)
            startAnimation(mainContainer, targetWidth, targetHeight)
        }
    }

    fun startAnimation(mainContainer: View, targetWidth: Int, targetHeight: Int) {
        val animator = ValueAnimator.ofInt(mainContainer.width, targetWidth)
        animator.addUpdateListener { valueAnimator ->
            val changeWidth = valueAnimator.animatedValue as Int
            mainContainer.layoutParams = mainContainer.layoutParams.apply {
                width = changeWidth
            }
        }
        animator.duration = ANIMATION_DURATION
        animator.start()

        val animator2 = ValueAnimator.ofInt(mainContainer.height, targetHeight)
        animator2.addUpdateListener { valueAnimator ->
            val changeHeight = valueAnimator.animatedValue as Int
            mainContainer.layoutParams = mainContainer.layoutParams.apply {
                height = changeHeight
            }
        }
        animator2.duration = ANIMATION_DURATION
        animator2.start()
    }

    override fun onBackPressed() {
        val firstFragmentParams = mViewDataBinding.firstContainer.getLayoutParams()
        val secondFregmentParams = mViewDataBinding.secondContainer.getLayoutParams()
        if (firstFragmentParams.width == mViewDataBinding.main.width) {
            changeHeightWidth(mViewDataBinding.firstContainer, mViewDataBinding.secondContainer)
        } else if (secondFregmentParams.width == mViewDataBinding.main.width) {
            changeHeightWidth(mViewDataBinding.secondContainer, mViewDataBinding.firstContainer)
        } else {
            super.onBackPressed()
        }
    }
}