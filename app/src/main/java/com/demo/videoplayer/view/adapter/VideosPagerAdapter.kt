package com.demo.videoplayer.view.adapter

import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.widget.VideoView
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.demo.videoplayer.data.VideoData
import com.demo.videoplayer.databinding.ItemVideosContainerBinding
import kotlin.math.roundToInt

class VideosPagerAdapter(
    private var items: ArrayList<VideoData>,
    private var screenWidth: Int,
    private var screenHeight: Int
) :
    RecyclerView.Adapter<VideosPagerAdapter.ViewHolder>(), VideoViewClickListener {

    private var screenProportion : Float = 0.0f
    private lateinit var gestureDetector: GestureDetector
    private lateinit var gestureListener: GestureListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemVideosContainerBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            com.demo.videoplayer.R.layout.item_videos_container,
            parent,
            false
        )
        screenProportion  = screenWidth / screenHeight.toFloat()
        gestureListener = GestureListener(screenWidth / 2.toFloat(), this)
        gestureDetector = GestureDetector(parent.context, gestureListener)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.tvVideoName.text = item.displayName

        holder.binding.videoView.setVideoPath(item.contentUri.toString())
        holder.binding.videoView.setOnPreparedListener { mp ->
            holder.binding.pbVideoPlayer.setVisibility(View.GONE)
            gestureListener.setVideoView(holder.binding.videoView)
            mp.start()
            val videoProportion = mp.videoWidth / mp.videoHeight.toFloat()

            val lp: ViewGroup.LayoutParams = holder.binding.videoView.getLayoutParams()
            if (videoProportion < screenProportion) {
                lp.height = screenHeight
                lp.width = (screenHeight.toFloat() / videoProportion).roundToInt()
            } else {
                lp.width = screenWidth
                lp.height = (screenWidth.toFloat() * videoProportion).roundToInt()
            }
            holder.binding.videoView.setLayoutParams(lp)

            //val scale = videoRatio / screenProportion

//            val videoHeight = (width * videoRatio) as Int

            //holder.binding.videoView.

            /*if (scale >= 1f) {
                holder.binding.videoView.setScaleX(scale)
            } else {
                holder.binding.videoView.setScaleY(1f / scale)
            }*/
        }
        holder.binding.videoView.setOnCompletionListener { mp -> mp.start() }

        holder.binding.clMain.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(view: View, event: MotionEvent): Boolean {
                return gestureDetector.onTouchEvent(event)
            }
        })
        holder.binding.clMain.setOnClickListener({
            /*if (holder.binding.videoView.isPlaying)
                holder.binding.videoView.pause()
            else
                holder.binding.videoView.start()*/
            /*if (holder.binding.videoView.currentPosition < 5000) {
                holder.binding.videoView.seekTo(0)
            } else {
                holder.binding.videoView.seekTo(holder.binding.videoView.currentPosition - 5000)
            }*/

            /*if (holder.binding.videoView.currentPosition + 5000 >= holder.binding.videoView.duration) {
                holder.binding.videoView.seekTo(holder.binding.videoView.duration - 1000)
            } else {
                holder.binding.videoView.seekTo(holder.binding.videoView.currentPosition + 5000)
            }*/
        })
    }

    override fun getItemCount(): Int = items.size

    private class GestureListener(
        private var centerX: Float,
        private var viewClickListener: VideoViewClickListener
    ) :
        SimpleOnGestureListener() {
        private var videoView: VideoView? = null

        fun setVideoView(videoView: VideoView) {
            this.videoView = videoView
        }

        override fun onDown(event: MotionEvent): Boolean {
            return true
        }

        override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
            if (videoView != null) {
                viewClickListener.viewClick(videoView!!)
            }
            return super.onSingleTapConfirmed(e)
        }

        // event when double tap occurs
        override fun onDoubleTap(event: MotionEvent): Boolean {
            if (videoView != null) {
                val x: Float = event.getX()
                if (x > centerX) {
                    viewClickListener.rightClick(videoView!!)
                } else {
                    viewClickListener.leftClick(videoView!!)
                }
            }
            return true
        }
    }

    class ViewHolder(@NonNull val binding: ItemVideosContainerBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun rightClick(videoView: VideoView) {
        if (videoView.currentPosition + 5000 >= videoView.duration) {
            videoView.seekTo(videoView.duration - 1000)
        } else {
            videoView.seekTo(videoView.currentPosition + 5000)
        }
    }

    override fun leftClick(videoView: VideoView) {
        if (videoView.currentPosition < 5000) {
            videoView.seekTo(0)
        } else {
            videoView.seekTo(videoView.currentPosition - 5000)
        }
    }

    override fun viewClick(videoView: VideoView) {
        if (videoView.isPlaying)
            videoView.pause()
        else
            videoView.start()
    }
}