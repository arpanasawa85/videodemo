package com.demo.videoplayer.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.demo.videoplayer.R
import com.demo.videoplayer.base.BaseFragment
import com.demo.videoplayer.data.VideoData
import com.demo.videoplayer.databinding.FragmentVideoListBinding
import com.demo.videoplayer.view.activity.VideoPlayerActivity
import com.demo.videoplayer.view.adapter.OnRecyclerItemClickedListener
import com.demo.videoplayer.view.adapter.VideoGalleryRecyclerViewAdapter
import com.demo.videoplayer.view.viewmodel.VideoGalleryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class VideoGalleryFragment : BaseFragment<FragmentVideoListBinding, VideoGalleryViewModel>() {

    private lateinit var adapter: VideoGalleryRecyclerViewAdapter

    companion object {
        fun newInstance() = VideoGalleryFragment()
    }

    override fun bindVariable() {
        mViewDataBinding.lifecycleOwner = viewLifecycleOwner
    }

    override fun getViewModel(): VideoGalleryViewModel {
        val viewModel: VideoGalleryViewModel by viewModel()
        return viewModel
    }

    override fun getLayout(): Int = R.layout.fragment_video_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.getVideoList()
        mViewModel.videoDataCompleted.observe(viewLifecycleOwner) { it ->
            mViewDataBinding!!.pbGallery.visibility = View.GONE
            if (it != null && it.size > 0) {
                mViewDataBinding!!.tvNoRecord.visibility = View.GONE
                adapter = VideoGalleryRecyclerViewAdapter(it)
                adapter.setonRecyclerItemClickedListener(object :
                    OnRecyclerItemClickedListener<VideoData> {
                    override fun onClicked(position: Int, item: VideoData) {
                        var playerIntent = Intent(activity, VideoPlayerActivity::class.java)
                        val bundle = Bundle()
                        with(bundle, {
                            putParcelableArrayList("videoData", it as ArrayList<out VideoData>)
                            putInt("position", position)
                        })
                        playerIntent.putExtras(bundle)
                        startActivity(playerIntent)
                    }
                })
                mViewDataBinding!!.videoListRecyclerView.adapter = adapter
            } else {
                mViewDataBinding!!.tvNoRecord.visibility = View.VISIBLE
            }
        }
    }
}