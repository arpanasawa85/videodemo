package com.demo.videoplayer.view.adapter

interface OnRecyclerItemClickedListener<T> {
    fun onClicked(position: Int, item: T)
}