package com.demo.videoplayer.view.activity

import android.os.Bundle
import android.util.DisplayMetrics
import com.demo.videoplayer.R
import com.demo.videoplayer.base.BaseActivity
import com.demo.videoplayer.data.VideoData
import com.demo.videoplayer.databinding.ActivityVideoPlayerBinding
import com.demo.videoplayer.view.adapter.VideosPagerAdapter
import com.demo.videoplayer.view.viewmodel.VideoPlayerViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class VideoPlayerActivity : BaseActivity<ActivityVideoPlayerBinding, VideoPlayerViewModel>() {
    override fun bindVariable() {
        mViewDataBinding.lifecycleOwner = this
    }

    override fun getViewModel(): VideoPlayerViewModel {
        val viewModel: VideoPlayerViewModel by viewModel()
        return viewModel
    }

    override fun getLayout(): Int = R.layout.activity_video_player

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var videoDatas: ArrayList<VideoData>? =
            this.intent.extras!!.getParcelableArrayList<VideoData>("videoData")
        var position = this.intent.extras!!.getInt("position")
        val displayMetrics = DisplayMetrics()
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        if (videoDatas != null) {
            mViewDataBinding.viewpagerVideos.adapter = VideosPagerAdapter(videoDatas, width, height)
            mViewDataBinding.viewpagerVideos.currentItem = position
        }

    }
}