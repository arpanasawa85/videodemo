package com.demo.videoplayer.view.activity

import android.os.Bundle
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.demo.videoplayer.R
import com.demo.videoplayer.base.BaseActivity
import com.demo.videoplayer.databinding.ActivityHomeBinding
import com.demo.videoplayer.view.fragment.VideoGalleryFragment
import com.demo.videoplayer.view.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>() {

    override fun bindVariable() {
        mViewDataBinding.lifecycleOwner = this
    }

    override fun getViewModel(): HomeViewModel {
        val viewModel: HomeViewModel by viewModel()
        return viewModel
    }

    override fun getLayout(): Int = R.layout.activity_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(mViewDataBinding!!.toolbar)
        if (savedInstanceState == null) {
            // Adds our fragment
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<VideoGalleryFragment>(R.id.fragment_container_view)
            }
        }
    }
}