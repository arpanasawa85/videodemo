package com.demo.videoplayer.view.activity

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.demo.videoplayer.R
import com.demo.videoplayer.base.BaseActivity
import com.demo.videoplayer.databinding.ActivitySplashBinding
import com.demo.videoplayer.databinding.GenericErrorDialogBinding
import com.demo.videoplayer.util.DialogHelper
import com.demo.videoplayer.view.viewmodel.SplashViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    private val TOTAL_TIME: Long = 2000
    private val READ_STORAGE_PERMISSION = 100

    override fun bindVariable() {
        mViewDataBinding.lifecycleOwner = this
    }

    override fun getViewModel(): SplashViewModel {
        val viewModel: SplashViewModel by viewModel()
        return viewModel
    }

    override fun getLayout(): Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handler()
    }

    private fun handler() {
        Handler(Looper.getMainLooper()).postDelayed({
            requestingPermission()
        }, TOTAL_TIME)
    }

    private fun goToNext() {
        var homeIntent = Intent(this, HomeActivity::class.java)
        startActivity(homeIntent)
        finishAfterTransition()
    }

    private fun showDialog() {
        val dialogView = GenericErrorDialogBinding.inflate(LayoutInflater.from(this))
        dialogView.dialogErrorText.text = getString(R.string.permission_need_msg)
        dialogView.dialogPositiveButton.setOnClickListener {
            DialogHelper.hide()
            requestingPermission()
        }
        dialogView.dialogNegativeButton.setOnClickListener {
            DialogHelper.hide()
            finish()
        }
        DialogHelper.showCustomDialog(
            context = this,
            view = dialogView.root
        )
    }

    private fun requestingPermission () {
        // Requesting the permission
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                showExplanation(getString(R.string.permission_needed), getString(R.string.permission_need_msg), Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_PERMISSION)
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    READ_STORAGE_PERMISSION
                )
            }
        } else {
            goToNext()
        }
    }

    private fun showExplanation(
        title: String,
        message: String,
        permission: String,
        permissionRequestCode: Int
    ) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok,
                DialogInterface.OnClickListener { dialog, id ->
                    requestPermission(
                        permission,
                        permissionRequestCode
                    )
                })
        builder.create().show()
    }

    private fun requestPermission(permissionName: String, permissionRequestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permissionName), permissionRequestCode)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == READ_STORAGE_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                goToNext()
            } else {
                Toast.makeText(this, getString(R.string.permission_needed), Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}